package rpg;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class mainStory {
	public int sum = 0;

	/*-------------------------------------------------------
	 *     最初のメタ発言
	 -------------------------------------------------------*/
    public static void title() throws IOException {
        System.out.println("* きょうこそ きになる あのこに あいにいこう。");
        System.out.println("* かくごは きまりましたか？");
        System.out.println("");
        System.out.println("             ❶きまった      ❷まだ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int text = Integer.parseInt(br.readLine());
        if (text == 1){
        	System.out.println("* あなたは さいしょの いっぽを ふみだした。");
        } else if (text == 2) {
            System.out.println("*「いくじなし」");
            System.out.println("* おこられたので あいに いくことにした。");
        } else {
            System.out.println("*「なんでやねん」");
            System.out.println("* はんこうてきな たいどをとり あきれられた。");
        }
    }

    /*-------------------------------------------------------
	 *     サイコロを回す
	 -------------------------------------------------------*/
    public void サイコロ() throws IOException {

    	//サイコロを50マスに到達するまで回す
        while(sum <= 50) {
        	System.out.println("");
        	System.out.println("* ❶で さいころを ふる。");
        	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        	//1を選択
			if(Integer.parseInt(br.readLine()) == 1) {

				//ランダムで数字を生成
				Random rnd = new Random();
				int koma = rnd.nextInt(6);
				//sumにたす
				sum = sum + koma;
				//sumが50以上になったら終わり
				if (sum >= 50) {
		        	System.out.println("");
		            System.out.println("");
		            System.out.println("* おめでとう。");
		            System.out.println("* きになる あのこに あえた。");
		            break;
		        }

				System.out.println("* サイコロの めは " + (koma+1) + " だった。");
				System.out.println();
				System.out.println("************************************************************");

				//敵キャラ紹介文
				story story = new story();
				System.out.println(story.story[sum]);

				//戦闘
				mainStory メインストーリー = new mainStory();
				メインストーリー.戦闘(sum);

				System.out.println("************************************************************");

			} else {
				System.out.println("* ❶いがいの せんたくの よちはない。");
				System.out.println("* はんせい するべきだ。");
			}
	    }
    }

    public void 戦闘(int sum) throws IOException {
    	story story = new story();
    	while (story.HP[sum] >= 0) {
    		System.out.println("");
    		System.out.println("             ❶たたかう      ❷ゆるす");
    		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    		int 選択 = Integer.parseInt(br.readLine());
    		if (選択 == 1) {

    			System.out.println("* " + "  「えいっ」");

				Random rnd = new Random();
				int atack = rnd.nextInt(3);
    			System.out.println("* " + story.勇者攻撃力[atack] + " ダメージ あたえた");

    			story.HP[sum] = story.HP[sum] - story.勇者攻撃力[atack];

    			if(story.HP[sum] >= 0) {
    				System.out.println(story.name[sum] + "「いたいよぉ」");

    				System.out.println("* " + story.name[sum] + "が はんげきしてきた。");
    				System.out.println("* " + story.name[sum] + "「どうだ」");
    				System.out.println("* " + story.攻撃力[sum] + "の ダメージ。");

    			} else {
    				System.out.println("* " + story.name[sum] + "「なんでぇ〜」");
    		    	System.out.println("");
    				System.out.println("* " + story.name[sum] + "をたおした");
    			}

    		}  else if (選択 == 2) {
    			System.out.println("* あなたは ゆるした。");
    			break;
    		} else {
    			System.out.println("* あなたは せんたく しなければならない。");
    			System.out.println("* じんせいは せんたくの れんぞくなのだから。");
    		}
    		break;
    	}

    }
}
